
Module: Exit Bee
Author: Exit Bee <info@exitbee.com>


Description
===========
Adds the Exit Bee tracking code to your website.

Requirements
============

* Exit Bee user account

Installation
============
Copy the 'exitbee' module directory in to your Drupal
sites/all/modules directory as usual.

Usage
=====
After enabling the module, the Exit Bee e-commere tracking code
will be added to your shop's checkout success pages.

Optionally, you can edit the Rules implementation which let you control
when to send the tracking code to Exit Bee.
This is useful if you redirect the user to a different page than the
default checkout complete page.
