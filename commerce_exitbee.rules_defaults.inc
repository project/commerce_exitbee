<?php

/**
 * @file
 * Adds default configuration for Exit Bee push order rule.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_exitbee_default_rules_configuration() {
  $rule = rules_reaction_rule();
  $rule->label = 'Send Exit Bee code on checkout completion';
  $rule->active = TRUE;
  $rule->event('commerce_checkout_complete')
    ->action('commerce_exitbee_send_order', array('commerce_order:select' => 'commerce_order')
  );
  $configs['commerce_exitbee_rule_eb'] = $rule;
  return $configs;
}
