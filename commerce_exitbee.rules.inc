<?php

/**
 * @file
 * Adds rules action which sends the exitbee ecommerce code.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_exitbee_rules_action_info() {
  return array(
    'commerce_exitbee_send_order' => array(
      'label' => t('Send order to Exit Bee'),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order in checkout'),
        ),
      ),
      'callback' => array(
        'execute' => 'commerce_exitbee_send_order',
      ),
    ),
  );
}
